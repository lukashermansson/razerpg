package me.lukasabc123abc.RaZeRPG;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;

import code.husky.mysql.MySQL;

public class LevelCheck extends BukkitRunnable{
	private final Plugin plugin;
	Connection c;
	MySQL MySQL;
	 
	public LevelCheck(Plugin plugin, Connection con, MySQL SQL) {
	this.plugin = plugin;
	c = con;
	MySQL = SQL;
	}
	
	 
	@SuppressWarnings("deprecation")
	public void run() {
		try {
			if(c.isClosed()){
				c = MySQL.openConnection();
			}
		} catch (ClassNotFoundException e1) {
			e1.printStackTrace();
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		for (Player player : plugin.getServer().getOnlinePlayers()){
			int playerxp = 0;
			int playerlevel = 0;
			int RequerdXp = 1;
			int diff = 0;
			try {
				Statement statement1 = c.createStatement();
				ResultSet res1 = statement1.executeQuery("SELECT uuid, level, xp FROM stats WHERE uuid = '" + player.getUniqueId() + "';");
				res1.next();
				playerxp = res1.getInt("xp");
				playerlevel = res1.getInt("level");
				Statement statement2 = c.createStatement();
				ResultSet res2 = statement2.executeQuery("SELECT level, xp FROM levels WHERE level = '" + playerlevel + "';");
				res2.next();
				RequerdXp = res2.getInt("xp");
				diff = playerxp - RequerdXp;
			} catch (Exception e) {
				e.printStackTrace();
			}
			if(playerxp >= RequerdXp){
				try {
					if(!(playerlevel >= Main.maxlevel-1)){
					Statement statement = c.createStatement();
					int clevel = playerlevel+1;
					statement.executeUpdate("UPDATE stats SET level='" + clevel + "' WHERE uuid='"+ player.getUniqueId() +"';");
					if(!(playerlevel+1 == Main.maxlevel)){
					Statement statement1 = c.createStatement();
					statement1.executeUpdate("UPDATE stats SET xp='" + diff + "' WHERE uuid='"+ player.getUniqueId() +"';");
					}else{
						Statement statement1 = c.createStatement();
						statement1.executeUpdate("UPDATE stats SET xp='" + 0 + "' WHERE uuid='"+ player.getUniqueId() +"';");
					}
					Main.onlevel(player, playerlevel);
					}else{
						//player wont level becuse of reaching max level
					}
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
            
        }

    }
}
