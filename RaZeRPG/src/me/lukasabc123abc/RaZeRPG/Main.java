package me.lukasabc123abc.RaZeRPG;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitScheduler;

import code.husky.mysql.MySQL;

public class Main extends JavaPlugin implements Listener{
	List<String> commands = new ArrayList<String>();
	String prefix = ChatColor.translateAlternateColorCodes('&', "&2RaZe&4RPG ");
	public static Main plugin;
	static int maxlevel = 100;
	public static void registerEvents(org.bukkit.plugin.Plugin plugin, Listener... listeners) {
		for (Listener listener : listeners) {
			Bukkit.getServer().getPluginManager().registerEvents(listener, plugin);
		}
	}
	MySQL MySQL;
	Connection c;

	@SuppressWarnings("deprecation")
	@Override
	public void onEnable() {
		plugin = this;
		MySQL = new MySQL(plugin, "81.233.70.252", "3306", "razze", "cabbe", "lukasgillarpaj");
		registerEvents(plugin, this);
		BukkitScheduler scheduler = Bukkit.getServer().getScheduler();
        
		try {
			c = MySQL.openConnection();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		scheduler.scheduleSyncRepeatingTask(this, new LevelCheck(plugin, c, MySQL), 0L, 20L);
		
		commands.add("/pl");
		commands.add("/help");
		commands.add("/essentias:help");
		commands.add("/plugin");
		commands.add("/bukkit:pl");
		commands.add("/bukkit:plugin");
		commands.add("/bukkit:version");
		commands.add("/bukkit:icanhasbukkit");
		commands.add("/version");
		commands.add("/icanhasbukkit");
	}
	
	


	@Override
	public void onDisable() {
		c = null;
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	 public void onPlayerChatEvent(AsyncPlayerChatEvent event) throws SQLException, ClassNotFoundException{
		if(c.isClosed()){
			c = MySQL.openConnection();
		}
		Statement statement1 = c.createStatement();
		ResultSet res1 = statement1.executeQuery("SELECT uuid, level FROM stats WHERE uuid = '" + event.getPlayer().getUniqueId() + "';");
		int playerlevel = res1.getInt("level");
		event.setFormat(event.getFormat().replace("ILevel", playerlevel + ""));
	}
	@EventHandler
	 public void OnjoinGame(PlayerJoinEvent event) throws SQLException, ClassNotFoundException{
		 if(c.isClosed()){
				c = MySQL.openConnection();
			}
		    Statement statement1 = c.createStatement();
			ResultSet res1 = statement1.executeQuery("SELECT uuid FROM stats WHERE uuid = '" + event.getPlayer().getUniqueId() + "';");
			if(res1.next() == false){
				//player is new and need a sql set up!
				Statement statement = c.createStatement();
				statement.executeUpdate("INSERT INTO `stats` (uuid, name) VALUES('" + event.getPlayer().getUniqueId() + "', '" + event.getPlayer().getName() +"')");
			}
	 }

	@EventHandler
	public void onCommandPreprocess(PlayerCommandPreprocessEvent event)
	{
		if(!event.getPlayer().hasPermission("RaZeRPG.blocked")){
		String message = event.getMessage();
		String[] array = message.split(" ");
		for (int i = 0; i < commands.size(); i++) {
			if(array[0].equalsIgnoreCase(commands.get(i))){
				event.setCancelled(true);
				event.getPlayer().sendMessage(prefix + ChatColor.translateAlternateColorCodes('&', "&cYou do not have access to view this page!"));
			}
		}
		}
	}
	@EventHandler
	public void onjoin(PlayerJoinEvent e){
		Player player = e.getPlayer();
		if(!player.hasPlayedBefore()){
			e.setJoinMessage("");
	    }else {
	    	if(e.getPlayer().hasPermission("join.owner")){
	    	    String message = "&a[+] &6" + e.getPlayer().getName();
	    	    e.setJoinMessage(ChatColor.translateAlternateColorCodes('&', message));
	       	}else if(e.getPlayer().hasPermission("join.admin")){
	       		String message = "&a[+] &c" + e.getPlayer().getName();
	    	    e.setJoinMessage(ChatColor.translateAlternateColorCodes('&', message));
	    	}else if(e.getPlayer().hasPermission("join.dev")){
	    		String message = "&a[+] &b" + e.getPlayer().getName();
		    	e.setJoinMessage(ChatColor.translateAlternateColorCodes('&', message));
	    	}else if(e.getPlayer().hasPermission("join.mod")){
	    		String message = "&a[+] &1" + e.getPlayer().getName();
		    	e.setJoinMessage(ChatColor.translateAlternateColorCodes('&', message));
	    	}else if(e.getPlayer().hasPermission("join.donator")){
	    		String message = "&a[+] &f" + e.getPlayer().getName();
		    	e.setJoinMessage(ChatColor.translateAlternateColorCodes('&', message));
	    	}else if(e.getPlayer().hasPermission("join.VIP")){
	    		String message = "&a[+] &5" + e.getPlayer().getName();
		    	e.setJoinMessage(ChatColor.translateAlternateColorCodes('&', message));
	    	}else if(e.getPlayer().hasPermission("join.VIP+")){
	    		String message = "&a[+] &d" + e.getPlayer().getName();
		    	e.setJoinMessage(ChatColor.translateAlternateColorCodes('&', message));
	    	}else{
	    		e.setJoinMessage("");
	    	}
	    }
	}
	@EventHandler
	public void onleave(PlayerQuitEvent e){
		e.setQuitMessage("");
	}
	
	public static void onlevel(Player player, int level) {
		if(level <= 70){
			//TODO: fucking medelande n�r man levlar
			Bukkit.getServer().broadcastMessage(ChatColor.translateAlternateColorCodes('&', "&a" + player.getName() + " Has reached level [Level]"));
		}else{
			//TODO:fuckingf medelande n�r man levlar
			player.sendMessage(ChatColor.translateAlternateColorCodes('&', "you leveeled up"));
		}
	}
	@EventHandler
	public void ondamage(EntityDamageEvent event){
		if(event.getEntity() instanceof Player){
			double damage = event.getFinalDamage();
			double chelth = 0;
			//ge damage via variable
			event.setCancelled(true);
			Statement statement1 = null;
			try {
				ResultSet res2 = statement1.executeQuery("SELECT uuid, chelth FROM stats WHERE uuid = '" + event.getEntity().getUniqueId() + "';");
				statement1 = c.createStatement();
				//TODO: helth grejor
				statement1.executeUpdate("UPDATE stats SET chelth='" + chelth + "' WHERE uuid='"+ event.getEntity().getUniqueId() +"';");
			} catch (SQLException e) {
				
				e.printStackTrace();
			}
		}	
	}
	
	//TODO: set damage
	@EventHandler
		 public void onDmg(EntityDamageByEntityEvent event) {
		        if (event.getDamager() instanceof Player) {
		        	org.bukkit.inventory.ItemStack sword = ((Player)event.getDamager()).getItemInHand();
		        	if (sword.hasItemMeta() && sword.getItemMeta().hasDisplayName()){
		        		if(c == null){
				 			try {
								MySQL.openConnection();
							} catch (ClassNotFoundException | SQLException e) {
								e.printStackTrace();
							}
				 		}
		        		Statement statement1;
						try {
							statement1 = c.createStatement();
							ResultSet res1 = statement1.executeQuery("SELECT name, damage FROM items;");
			    			while (res1.next()) {
			    				if(sword.getItemMeta().getDisplayName().contains(ChatColor.translateAlternateColorCodes('&', res1.getString("name")))){
			    					event.setDamage(res1.getDouble("damage"));
			    				}
			    			}
						} catch (SQLException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
		    			
		        	}else{
		        	event.setDamage(0.0);
		        	}
		        }
		}
		@Override @EventHandler(priority = EventPriority.HIGHEST)
		public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		 	if (cmd.getName().equalsIgnoreCase("giveitem")) {
		 		Player player = Bukkit.getPlayerExact(args[0]);
		 		if(!(player == null)){
		 		if(c == null){
		 			try {
						MySQL.openConnection();
					} catch (ClassNotFoundException | SQLException e) {
						e.printStackTrace();
					}
		 		}
				try {
					Statement statement1;
					statement1 = c.createStatement(); //TODO: error 
					ResultSet res1 = statement1.executeQuery("SELECT name, material, give FROM items;");
	    			while (res1.next()) {
	    				if(args[1].equalsIgnoreCase(res1.getString("give"))){
	    					ItemStack stack = new ItemStack(Material.getMaterial(res1.getString("material")));
	    					ItemMeta meta = stack.getItemMeta();
	    					meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', res1.getString("name")));
	    					stack.setItemMeta(meta);
	    					player.getInventory().addItem(stack);
	    				}else{
	    					sender.sendMessage("invalid item");
	    				}
	    			}
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
    			
		 	}else{
		 		sender.sendMessage("player offline");
		 	}
		 	}
		return false;
		}
		@EventHandler
		public void armorcalc(EntityDamageEvent event){
			if(event.getEntity() instanceof Player){
				Player player = (Player) event.getEntity();
				ItemStack[] invArmour  = player.getInventory().getArmorContents();
				Statement statement1;
				ResultSet res1 = null;
				double totaldef = 0;
				try {
					statement1 = c.createStatement();
					res1 = statement1.executeQuery("SELECT name, defence FROM items;");
					while (res1.next()) {}
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				for (int i = 0; i < invArmour.length; i++) {
					try {
						while (res1.next()) {
							if(invArmour[i].getItemMeta().getDisplayName().contains(ChatColor.translateAlternateColorCodes('&', res1.getString("name")))){
								double value = res1.getDouble("defence") / 1000;
								event.setDamage(event.getDamage() - (event.getDamage() * value));
								totaldef = totaldef+value;
							}
						}
					} catch (SQLException e) {
						
						e.printStackTrace();
					}
				}
		}
	}
}
